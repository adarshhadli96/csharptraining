﻿using StudentInformation.Coderefactored.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.Models
{
    public class Address : BaseEntity
    {
        public Address()
        {

        }

        public string? City { get; set; }
        public string? State { get; set; }
        public string? Country { get; set; }
        public PinCode PostalCode { get; set; }



        public static Address Create(int id,string city, string state, string Country)
        {
            Address address = new Address()
            {
                Id = id,
                City = city,
                State = state,
                Country = Country,
                
            };
            return address;
        }

        public class PinCode
        {
           public int Id { get; set; }
            public string? Code { get; set; }

           
        }

    }


}
