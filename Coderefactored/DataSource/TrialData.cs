﻿using StudentInformation.Coderefactored.Models;



namespace StudentInformation.Coderefactored.DataSource
{


    public static class TrialData
    {
      private static List<StudentInformation.Coderefactored.Models.Student> lststudents = new List<StudentInformation.Coderefactored.Models.Student>();

       private static List<Department> lstdepartments = new List<Department>();
 
        private static List<Address> lstaddresses = new List<Address>();
        


        public static List<StudentInformation.Coderefactored.Models.Student> GetStudents()
        {
            StudentInformation.Coderefactored.Models.Student studentobj1 = StudentInformation.Coderefactored.Models.Student.Create(1, "Raghu", "P", 21, new DateTime(2016, 06, 16), true, "N", 1,1, 583238);
            StudentInformation.Coderefactored.Models.Student studentobj2 = StudentInformation.Coderefactored.Models.Student.Create(2, "Rakesh", "R", 22, new DateTime(2017, 07, 17), true, "N", 2,3, 583238);
            StudentInformation.Coderefactored.Models.Student studentobj3 = StudentInformation.Coderefactored.Models.Student.Create(3, "Ravi", "S", 26, new DateTime(2018, 08, 18), true, "N", 3,4, 583238);
            StudentInformation.Coderefactored.Models.Student studentobj4 = StudentInformation.Coderefactored.Models.Student.Create(4, "Shankar", "D", 24, new DateTime(2019, 09, 18), true, "N", 4,2, 583238);
            StudentInformation.Coderefactored.Models.Student studentobj5 = StudentInformation.Coderefactored.Models.Student.Create(5, "Abhishek", "G", 23, new DateTime(2018, 08, 18), true, "N", 1,4, 583238);
            StudentInformation.Coderefactored.Models.Student studentobj6 = StudentInformation.Coderefactored.Models.Student.Create(6, "Arun", "K", 25, new DateTime(2018, 08, 18), true, "N", 3,2,583238);
            
            lststudents.Add(studentobj1);
            lststudents.Add(studentobj2);
            lststudents.Add(studentobj3);
            lststudents.Add(studentobj4);
            lststudents.Add(studentobj5);
            lststudents.Add(studentobj6);
            
            return lststudents;
        }

        public static List<Department> GetDepartments()
        {
            Department departmentobj1 = Department.Create(1, "Dep1");
            Department departmentobj2 = Department.Create(2, "Dep2");
            Department departmentobj3 = Department.Create(3, "Dep3");
            Department departmentobj4 = Department.Create(4, "Dep4");
            
            lstdepartments.Add(departmentobj1);
            lstdepartments.Add(departmentobj2);
            lstdepartments.Add(departmentobj3);
            lstdepartments.Add(departmentobj4);
           
            return lstdepartments;
        }

        public static List<Address> GetAddress()
        {

            Address addressobj1 = Address.Create(1, "Bangalore", "Karnataka", "India");
            Address addressobj2 = Address.Create(2, "Chennai", "Tamilnadu", "India");
            Address addressobj3 = Address.Create(3, "Vizag", "Andrapradesh","India");
            Address addressobj4 = Address.Create(4, "Hyderabad","Telangana","India");

            lstaddresses.Add(addressobj1);
            lstaddresses.Add(addressobj2);
            lstaddresses.Add(addressobj3);
            lstaddresses.Add(addressobj4);

            return lstaddresses;
        }

    }
}
