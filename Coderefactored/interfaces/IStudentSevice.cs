﻿using StudentInformation.Coderefactored.Sevices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.interfaces
{
    internal interface Istudentsevice
    {
        public void Displaystudentdetails(StudentInformation.Coderefactored.Models.Student student);
        public void Displaystudentdetails(List<StudentInformation.Coderefactored.Models.Student> students);
        public List<StudentInformation.Coderefactored.Models.Student> GetStudentsdetails();
        public void Displaystudentfilterdetails(List<StudentInformation.Coderefactored.Models.Student> students);
    }
}
