﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.interfaces
{
    internal interface IstudentRepo
    {
        public void Create(StudentInformation.Coderefactored.Models.Student student);
        public void Update(StudentInformation.Coderefactored.Models.Student student);
        public List<StudentInformation.Coderefactored.Models.Student> Read();

        public void Delete(int id);
    }
}
