﻿using StudentInformation.Coderefactored.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.DTOs
{
    public class StudentDto
    {
        public int StudentId { get; set; }
        public string? Name { get; set; }
        public int Age { get; set; }
        public string? DepartmentName { get; set; }

        public Address? Address { get; set; }
        public List<Address?>? Addresses { get; set; } 


       
        

       

        //public string? City { get; set; }
        //public string? State { get; set; }
        //public string? Country { get; set; }
    }


}
