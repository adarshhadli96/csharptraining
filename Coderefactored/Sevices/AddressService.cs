﻿using StudentInformation.Coderefactored.DataSource;
using StudentInformation.Coderefactored.Models;
using StudentInformation.Coderefactored.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.Sevices
{
    public class AddressService : BaseEntity
    {
        public List<Address> GetAddressdetails()
        {
            return TrialData.GetAddress();
        }
    }
}
